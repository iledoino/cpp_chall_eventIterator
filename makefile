COMPILER = g++
CFLAGS 	 = -I./src -O2 -lm -Wall -std=gnu++11
COBJ     = ./obj/EventIterator.o ./obj/EventStore.o

all: ./obj/EventIterator.o ./obj/EventStore.o
	mkdir -p obj
	make ./obj/EventIterator.o
	make ./obj/EventStore.o

./obj/Event.o: ./src/Event.cpp ./src/Event.h
	$(COMPILER) $(CFLAGS) -c ./src/Event.cpp -o ./obj/Event.o

./obj/EventIterator.o: ./src/EventIterator.cpp ./src/EventIterator.h ./obj/Event.o
	$(COMPILER) $(CFLAGS) -c ./src/EventIterator.cpp -o ./obj/EventIterator.o

./obj/EventStore.o: ./src/EventStore.cpp ./src/EventStore.h ./obj/EventIterator.o
	$(COMPILER) $(CFLAGS) -c ./src/EventStore.cpp -o ./obj/EventStore.o

clean:
	rm ./obj/*.o
