#include "EventIterator.h"

std::map<std::size_t, std::shared_ptr<Event>>::iterator
        EventIterator::default_iter
        = TimeEventMapIterator{};

EventIterator::EventIterator(TimeEventMapPtr timeEventMapPtr_p,
        std::size_t startTime_p, std::size_t endTime_p)
    : timeEventMapPtr(timeEventMapPtr_p)
    , startTime(startTime_p)
    , endTime(endTime_p)
    , status(false)
    , initialized(false)
    , iter(timeEventMapPtr_p ? timeEventMapPtr_p->lower_bound(startTime_p) :
                               default_iter)
    , end(timeEventMapPtr_p ? timeEventMapPtr_p->upper_bound(endTime_p) :
                              default_iter)
{
}

bool
EventIterator::isValidIter(
        TimeEventMapIterator iter_p, TimeEventMapIterator end_p)
{
    return (iter_p != end_p) && (not(iter_p->second));
}

std::map<std::size_t, std::shared_ptr<Event>>::iterator
EventIterator::findValidIter(
        TimeEventMapIterator iter_p, TimeEventMapIterator end_p)
{
    while (isValidIter(iter_p, end_p))
        iter_p++;
    return iter_p;
}

bool
EventIterator::iterIsEnd() const
{
    return iter == end;
}

bool
EventIterator::moveNext()
{
    /*
     * Execution enters the if only when moveNext has already been called
     * and last call returned a false status, or if current iter has already
     * reached the end
     */
    if ((not timeEventMapPtr) || (initialized && not status) || iterIsEnd()) {
        initialized = true;
        status = false;
        return status;
    }

    /*
     * Execution enters the if only when moveNext has already been called,
     * current iter is valid, and current iter is not the end
     *   -Move to next valid iterator
     * Execution enters the else only when moveNext has not yet been called
     * and current iter is not the end
     *   -Find an iterator that is either the end iterator, or it is a valid
     *    iterator whose time stamp is equal or greater than startTime
     */
    if (initialized)
        iter = findValidIter(++iter, end);
    else {
        initialized = true;
        iter = findValidIter(iter, end);
    }

    /*
     * At this point, either iter is valid and between start and end time,
     * or it is valid and bound above by endTime, or it is the end iterator
     */
    status = not iterIsEnd();
    return status;
}

std::shared_ptr<Event>
EventIterator::current()
{
    if (not status)
        throw IllegalStateException();

    return iter->second;
}

void
EventIterator::remove()
{
    if (not status)
        throw IllegalStateException();

    iter->second = std::shared_ptr<Event>();

    // Should remove also moveNext ?
    // moveNext();
}
