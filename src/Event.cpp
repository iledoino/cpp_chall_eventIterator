#include "Event.h"

Event::Event(std::string type_p, std::size_t timestamp_p)
    : type_m(type_p), timestamp_m(timestamp_p)
{
}

Event::Event(std::istream& input) : type_m(), timestamp_m()
{
    std::string voidstr;
    // PS: To avoid problems with reading type, it should be an id or an enum
    input >> voidstr >> type_m >> timestamp_m;
}

std::ostream&
operator<<(std::ostream& output, const Event& e)
{
    output << "Event: " << e.type() << " " << e.timestamp();
    return output;
}
