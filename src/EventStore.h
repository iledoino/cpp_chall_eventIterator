#ifndef MAIN_EVENTSTORE_H
#define MAIN_EVENTSTORE_H

#include "Event.h"
#include "EventIterator.h"
#include <map>
#include <list>
#include <memory>
#include <unordered_map>

/**
 * An @c EventStore is an abstraction of an @c Event store.
 * Events may be stored in memory, data files, a database, on a remote
 * server, etc. For this challenge, you should implement an in-memory
 * event event store.
 */
class EventStore
{
public:
    EventStore() = default;

    /**
     * Stores an event
     *
     * @param @c Event
     */
    void insert(std::string type, std::size_t timestamp);
    virtual void insert(std::shared_ptr<Event> event_ptr);

    /**
     * Removes all events of specific type.
     *
     * @param type
     */
    void removeAll(std::string type);

    /**
     * Retrieves an iterator for events based on their type and timestamp.
     *
     * @param type      The type we are querying for.
     * @param startTime Start timestamp (inclusive).
     * @param endTime   End timestamp (exclusive).
     * @return An iterator where all its events have same type as
     * {@param type} and timestamp between {@param startTime}
     * (inclusive) and {@param endTime} (exclusive).
     */
    EventIterator
    query(std::string type, std::size_t startTime, std::size_t endTime);

    /**
     * Rehashes the maps used internally, so that null pointers do not
     * slow dow the use of the maps
     */
    void rehash();

    friend std::ostream& operator<<(std::ostream& output, const EventStore& s);

private:
    using TimeEventMap = typename std::map<std::size_t, std::shared_ptr<Event>>;
    using TimeEventMapPtr = typename std::shared_ptr<TimeEventMap>;
    using KeyEventMap =
            typename std::unordered_map<std::string, TimeEventMapPtr>;

    KeyEventMap table;
    std::list<std::string> rehashKeys;
};

#endif /* MAIN_EVENTSTORE_H */
