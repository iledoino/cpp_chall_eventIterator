#include "EventStore.h"

void
EventStore::insert(std::string type_p, std::size_t timestamp_p)
{
    auto event_ptr_p = std::make_shared<Event>(type_p, timestamp_p);
    insert(event_ptr_p);
}

void
EventStore::insert(std::shared_ptr<Event> event_ptr_p)
{
    if (not event_ptr_p)
        return;

    if (table.find(event_ptr_p->type()) == table.end())
        table[event_ptr_p->type()] = std::make_shared<TimeEventMap>();

    // Could it happen that two insertions would be made with same timestamp???
    (*table[event_ptr_p->type()])[event_ptr_p->timestamp()] = event_ptr_p;
}

void
EventStore::removeAll(std::string type_p)
{
    table.erase(type_p);
}

EventIterator
EventStore::query(
        std::string type_p, std::size_t startTime_p, std::size_t endTime_p)
{
    TimeEventMapPtr timeEventMapPtr;
    auto iter = table.find(type_p);
    if (iter != table.end()) {
        timeEventMapPtr = iter->second;
        rehashKeys.emplace_back(type_p);
    }

    return EventIterator(timeEventMapPtr, startTime_p, endTime_p);
}

void
EventStore::rehash()
{
    for (auto key: rehashKeys)    {
        auto iterk = table.find(key);
        if (iterk == table.end())
            continue;
        auto ptr = iterk->second;
        if (ptr)
        {
            for(auto iter=ptr->begin() ; iter!=ptr->end() ; )
                if (not iter->second) {
                    auto iterd = iter++;
                    ptr->erase(iterd->first);
                }
                else
                    iter++;
        }
    }
    rehashKeys.clear();
}

std::ostream&
operator<<(std::ostream& output, const EventStore& s)
{
    output << "EventStore: " << s.table.size() << "\n";
    for (auto& pair : s.table) {
        if (not pair.second)
            output << "\t" << pair.first << " " << 0 << "\n";
        else {
            output << "\t" << pair.first << " " << pair.second->size() << "\n";
            for (auto& pair_e : *(pair.second))
                if (pair_e.second)
                    output << "\t\t" << *(pair_e.second) << "\n";
        }
    }
    return output;
}
