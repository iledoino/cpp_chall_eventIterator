#ifndef MAIN_EVENTITERATOR_H
#define MAIN_EVENTITERATOR_H

#include "Event.h"
#include <map>
#include <memory>
#include <stdexcept>

/**
 * An iterator over an @c Event collection.
 */
class EventIterator
{
public:
    using TimeEventMap = typename std::map<std::size_t, std::shared_ptr<Event>>;
    using TimeEventMapPtr = typename std::shared_ptr<TimeEventMap>;
    using TimeEventMapIterator = TimeEventMap::iterator;

    struct IllegalStateException : public std::exception
    {
        const char* what() const throw()
        {
            return "EventIterator: illegal iterator";
        }
    };

    EventIterator(TimeEventMapPtr timeEventMapPtr, std::size_t startTime,
            std::size_t endTime);

    /**
     * Move the iterator to the next event, if any.
     *
     * @return false if the iterator has reached the end, true otherwise.
     */
    bool moveNext();

    /**
     * Gets the current event ref'd by this iterator.
     *
     * @return the event itself.
     * @throws IllegalStateException if {@link #moveNext} was never called
     *                               or its last result was {@code false}.
     */
    std::shared_ptr<Event> current();

    /**
     * Remove current event from its store.
     *
     * @throws IllegalStateException if {@link #moveNext} was never called
     *                               or its last result was {@code false}.
     */
    void remove();

private:
    static bool
    isValidIter(TimeEventMapIterator iter, TimeEventMapIterator end);
    static TimeEventMapIterator
    findValidIter(TimeEventMapIterator iter, TimeEventMapIterator end);
    bool iterIsEnd() const;

    static TimeEventMapIterator default_iter;

    TimeEventMapPtr timeEventMapPtr;
    std::size_t startTime, endTime;
    bool status, initialized;
    TimeEventMapIterator iter;
    TimeEventMapIterator end;
};

#endif /* MAIN_EVENTITERATOR_H */
