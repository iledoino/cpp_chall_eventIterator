#ifndef MAIN_EVENT_H
#define MAIN_EVENT_H

#include <iostream>
#include <string>

/**
 * @c Event class: the core definition of an event
 */
class Event
{
public:
    Event(std::string type_p, std::size_t timestamp_p);
    Event(std::istream& input);

    const std::string type() const { return type_m; }
    const std::size_t timestamp() const { return timestamp_m; }

    friend std::ostream& operator<<(std::ostream& output, const Event& e);

private:
    std::string type_m;
    std::size_t timestamp_m;
};

#endif /* MAIN_EVENT_H */
