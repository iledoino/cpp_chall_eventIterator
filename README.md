# Implement EventStore

In this challenge one should create a class that implements the `EventStore` interface.
 
```c++
class EventStore
{
public:
    virtual void insert(std::shared_ptr<Event> event_ptr) = 0;

    virtual void removeAll(std::string type) = 0;

    virtual EventIterator query(std::string type, std::size_t startTime, std::size_t endTime) = 0;
}
```

The implementation should store events in memory, using any data structures considered to be fit for the task. The required behavior for the interface is described in the provided code comments. Please see `EventStore` and `EventIterator` interfaces inside the `src/main` directory.
 
The implementation should be correct, fast, memory-efficient, and thread-safe. One may consider that insertions, deletions, queries, and iterations will happen frequently and concurrently. This will be a system hotspot. Optimize at will. 

It is expected that a candidate do:
* Write tests;
* Provide some evidence of thread-safety;
* Justify design choices, arguing about costs and benefits involved. Write those as comments inline or provide a separate document summarizing those choices;
* Write all code and documentation in english.
  
External libraries may be used, but their use has to be properly justified as well.
 

# Implementation of EventStore

This document is intended to discuss the proposed solution to the problem of managing an event store. 

## Data Structure

### Event

Events are stored by means of smart pointers. This choice is intended to  
	-allow for *child* classes to store extra information if needed: pointers of base classes can hold addresses of their child classes;
	-save as much memory as possible: memory is automatically handled by smart pointers, being freed as soon these pointers no longer hold addresses to it;
	-ease the removal of events: to erase an event, one simply resets the smart pointer holding its address. Efectivelly removing the pointer could invalidate an iterator pointing to it.

### EventIterator

Event iterators are wrappers for dealing with a *C++* standard map. This data structure associates a *timestamp* to a smart pointer for an *Event*, where the first of these is ordered in the data structure. The standard implementation of the map uses [binary search trees](https://en.wikipedia.org/wiki/Binary_search_tree), whose properties that justify its choice for this purpose are
	-the data structure allows for fast iteration: since by design a removal of a single event requires first quering for a range and then iterating through it until finding the desired event, it is desirable that iterating is fast;
	-the data structure is ordered: this facilitates quering events at a given timestamp range.

### EventStore

The event store holds a *C++* standard unordered map. The keys of the map are the keys of events, and its values are smart pointers for the ordered maps mentioned in the *EventIterator* section. The unordered map is chosen because its internal implementation uses [separate chaining](https://en.wikipedia.org/wiki/Hash_table#Separate_chaining). This type of data structure is most favorable in this case because
	-it allows for fast search and insertion of keys: in both cases the complexity should be constant in average, with the worst case being linear with size. However, insertion may trigger a rehashing of the structure, which is linear in average, but quadratic in the worst case scenario;
	-it allows for fast removall of keys: removing one key has constant complexity.

## Requirements met in this implementation

### Correctness

For the purpose of verifying correctness, a simple test which exemplifies all features of the original design is proposed in the `$(EVENTS_PATH)/tests/mainsimple.cpp` file. Instructions for running the test are given under *Compilation* section. 

### Speed

The implementation is very fast for inserting and removing specific events, for iterating and for removing keys. However, the complexity of querying is logarithmic with size, which may become a big problem given the choice of smart pointers for storing events. For the purpose of diminishing this problem, the author has included a `void rehash();` function in class `EventStore` which searches for empty smart pointers and removes them.

### Memory Efficiency

Since smart pointers are used to store Events, the implementation is as memory efficient as it can be, as far as the author is able to see. 

### Thread Safety

The author has not assessed thread safety in this implementation. However, plans as how to procceed have been developed. Here are some of his ideas for dealing with the problem:
	-Assuming that *OpenMP* threads are created, and that the `EventStore` instance is shared amongst the threads, a few `critical` blocks could be inserted to coordinate essential tasks:
		-A `critical` block would be necessary when adding a new key, but not necessarily a new event. The block should be *named* after the key in order to allow for the most concurrency.
		-A `critical` block would not be necessary in a call to `removeAll`. Although operations made using an `EventIterator` instance with the same key would end up being a waste of computation, this could be a rare event and also one that does not affect correctness. 
		-A `critical` block would be necessary in the *rehash* function. The function should also be positioned strategically since it could invalidate iterators active in different threads.
		-No `critical` block would be needed inside `EventIterator` class, since by design, even the `current` function returns a shared pointer to an event. 

## Compilation

To compile the code, the author has provided simple `makefile` files under `$(EVENTS_PATH)/` and `$(EVENTS_PATH)/tests/` directories. The first of these compiles the source code and the second compiles some tests, described below.

## Tests

Two main tests have been devised to confront the implementation. They are discussed below.

### Correctness

This test is implemented in `$(EVENTS_PATH)/tests/mainsimple.cpp` file. It consists of a sequence of commands issued in a given sequence, so that at each step one can look at ouput printed to the screen in order to verify the commands do what they should. To compile the test, one should first execute `make` under `$(EVENTS_PATH)/`, and then under `$(EVENTS_PATH)/tests/`. After that, hitting `make run` in the terminal executes the test. One should look at the screen and compare the output with the expected response infered by analysing the code in order to verify correctness. 

### Performance 

This test is implemented in `$(EVENTS_PATH)/tests/mainperf.cpp` with the goal of assessing the performance of the proposed solution. The application uses some randomness to execute operations that totalize a value provided by the user. By repeated executions increasing the size of some operations, one can determine their time complexity. The author has provided `Python` scripts which show graphs of time complexity obtained by running the tests in his machine. The scripts are named `$(EVENTS_PATH)/tests/insert.py` and  `$(EVENTS_PATH)/tests/query_and_removal.py`, and the data they use are calculated by executing `make insert` and `make query_and_removal` in the terminal. 

