import numpy as np
import matplotlib.pyplot as plt

data=np.array([
[10000     , 0.01   , 0.01   , 0.01  ],
[100000    , 0.09   , 0.08   , 0.08  ],
[1000000   , 0.86   , 0.81   , 0.81  ],
[10000000  , 10.05  , 9.99   , 9.87  ],
[100000000 , 110.79 , 111.82 , 110.13]]);

data_mean = np.mean(data[:,1:], axis=1);
plt.plot(data[:,0], data_mean);
plt.xlabel('number of inserts');
plt.ylabel('mean elapsed time');
plt.show();
