import numpy as np
import matplotlib.pyplot as plt

data=np.array([
[1000, 0.01, 0.01, 0.01         ],
[10000, 0.09, 0.09, 0.09        ],
[100000, 1.47, 1.56, 1.47       ],
[250000, 12.82, 14.93, 15.85    ],
[500000, 88.06, 85.24, 86.97    ],
[750000, 230.66, 224.90, 225.56 ],
[1000000, 434.13, 452.21, 451.48]]);

data_mean = np.mean(data[:,1:], axis=1);
plt.plot(data[:,0], data_mean);
plt.xlabel('number of queries/removals');
plt.ylabel('mean elapsed time');
plt.show();
