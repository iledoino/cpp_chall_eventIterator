#include "EventStore.h"
#include <iostream>
#include <random>
#include  <cstdlib>

int
evolve(std::size_t& timestamp)
{
    timestamp++;
    return timestamp;
}

double
prob(std::uniform_real_distribution<>& dis, std::mt19937& gen)
{
    return dis(gen);
}

int
main(int argc, char** argv)
{
    std::size_t nkeys = 1e2;
    std::size_t inserts = 1e8;
    std::size_t removealls = 1e3;
    std::size_t queries = 1e4;
    std::size_t nremoves = 10;
    EventStore store;

    if (argc > 5)
    {
        nkeys = std::atol(argv[1]);
        inserts = std::atol(argv[2]);
        removealls = std::atol(argv[3]);
        queries = std::atol(argv[4]);
        nremoves = std::atol(argv[5]);
    }

    std::size_t timestamp = 0;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0.0, 1.0);

    while(inserts>0 or removealls>0 or queries>0)
    {
        double p=prob(dis, gen);
        std::string key = std::to_string(static_cast<std::size_t>(prob(dis, gen)*nkeys)%nkeys);
        if ((p<1.5/3.0) and (inserts>0))
        {
            store.insert(key,evolve(timestamp));
            inserts--;
        }
        else if ((p<2.5/3.0) and (queries>0))
        {
            auto iter=store.query(key, 1, timestamp);
            do {
                p = prob(dis, gen);
            }
            while(p<1.0e-3);
            int removes = static_cast<int>(nremoves);
            bool valid_e = true;
            while(removes > 0 and valid_e)
            {
                do
                {
                    valid_e = iter.moveNext();
                }
                while(p>prob(dis, gen) and valid_e);
                if (valid_e)
                {
                    iter.remove();
                    removes--;
                }
            }
            queries--;
            store.rehash();
        }
        else if (removealls>0)
        {
            store.removeAll(key);
            removealls--;
        }
    }

    return 0;
}
