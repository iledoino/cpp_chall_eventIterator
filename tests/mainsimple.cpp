#include "EventStore.h"
#include <iostream>

int
evolve(std::size_t& timestamp)
{
    timestamp++;
    return timestamp;
}

/*
 * This simple test attempts at verifying the basic features the event
 * store should have
 */
int
main(int argc, char** argv)
{
    EventStore store;

    std::string key1{ "FULANO" }, key2{ "CICLANO" }, key3{ "BELTRANO" };
    std::size_t timestamp{ 0 };

    std::cout << "Adding 5 entries to store: " << std::endl;
    store.insert(key1, evolve(timestamp));
    store.insert(key1, evolve(timestamp));
    store.insert(key2, evolve(timestamp));
    store.insert(key3, evolve(timestamp));
    store.insert(key3, evolve(timestamp));
    std::cout << store << std::endl;

    std::cout << "Removing " << key2 << " entries from store: " << std::endl;
    try {
        store.removeAll(key2);
    }
    catch (const std::exception& e) {
        std::cout << "\t" << e.what() << std::endl;
    }
    std::cout << store << std::endl;

    std::cout << "Adding 5 more entries to store: " << std::endl;
    store.insert(key1, evolve(timestamp));
    store.insert(key2, evolve(timestamp));
    store.insert(key2, evolve(timestamp));
    store.insert(key2, evolve(timestamp));
    store.insert(key3, evolve(timestamp));
    std::cout << store << std::endl;

    std::size_t startTime{ 1 }, endTime{ 5 };
    std::cout << "Quering for " << key3 << ", events between [" << startTime
              << "," << endTime << "), and removing the first event"
              << std::endl;
    auto iter = store.query(key3, startTime, endTime);
    std::cout << "\tAttempting at getting current event without a call to "
                 "moveNext: "
              << std::endl;
    try {
        iter.current();
    }
    catch (const std::exception& e) {
        std::cout << "\t" << e.what() << std::endl;
    }
    std::cout << "\tAttempting at removing current event without a call to "
                 "moveNext: "
              << std::endl;
    try {
        iter.remove();
    }
    catch (const std::exception& e) {
        std::cout << "\t" << e.what() << std::endl;
    }
    std::cout
            << "\tAttempting at removing first event after a call to moveNext: "
            << std::endl;
    if (iter.moveNext()) {
        auto eventPtr = iter.current();
        if (eventPtr)
            std::cout << "\t\tRemoving event: " << *eventPtr << std::endl;
        iter.remove();
    }
    else
        std::cout << "\t\tReceive false from moveNext" << std::endl;
    std::cout << "Moving next up until the end and attempting at getting event "
                 "and removing it: "
              << std::endl;
    while (iter.moveNext()) {
        auto eventPtr = iter.current();
        if (eventPtr)
            std::cout << "\tAt event: " << *eventPtr << std::endl;
    }
    try {
        iter.current();
    }
    catch (const std::exception& e) {
        std::cout << "\t" << e.what() << std::endl;
    }
    try {
        iter.remove();
    }
    catch (const std::exception& e) {
        std::cout << "\t" << e.what() << std::endl;
    }
    std::cout << store << std::endl;

    return 0;
}
